process.env.NODE_ENV = 'test';

const request = require('supertest-as-promised');
const assert = require('chai').assert;
const jwt = require('jsonwebtoken');

const config = require('../config/env');
const app = require('../config/express');
const User = require('../api/models/User');

describe('User', () => {

	let token = '';

	before(async () => {
		await User.sync({ force: true });
		await User.create({
			username: 'Demo',
			password: '1234',
		});
		await User.create({
			username: 'Amit',
			password: '1234',
		});
		token = await jwt.sign({ id: 1 }, config.jwt.jwtSecret, { expiresIn: config.jwt.jwtDuration });
	});

	describe('GET /users', () => {
		it('It should GET all the users', (done) => {
			request(app)
				.get('/users')
				.set('Authorization', `Bearer ${token}`)
				.expect(200)
				.then((res) => {
					assert.typeOf(res.body, 'array');
					assert.equal(res.body.length, 2);
					done();
				});
		});
	});

	describe('POST /users', () => {
		it('It should create a new user', (done) => {
			request(app)
				.post('/users')
				.send({
					username: 'UserOne',
					password: '1234',
				})
				.expect(201)
				.then((res) => {
					assert.equal(res.body.username, 'UserOne');
					done();
				});
		})
	});

	describe('GET /users/:userId', () => {
		it('It should retrieve the user with id 1', (done) => {
			request(app)
				.get('/users/3')
				.set('Authorization', `Bearer ${token}`)
				.expect(200)
				.then((res) => {
					assert.equal(res.body.username, 'UserOne');
					done();
				});
		});
	});

	describe('PUT /users/:userId', () => {
		it('It should update user "Demo" to "Updated Demo"', (done) => {
			request(app)
				.put('/users/2')
				.set('Authorization', `Bearer ${token}`)
				.send({
					username: 'UserOne',
				})
				.expect(201, done());
		});
	});

	describe('DELETE /users/:userId', () => {
		it('It should delete user with id 3', (done) => {
			request(app)
				.delete('/users/3')
				.set('Authorization', `Bearer ${token}`)
				.expect(204, done());
		});
	});

});
