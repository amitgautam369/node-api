1. Install NPM dependencies

```
$ npm install
```
  
2. Make sure you have a MySQL DB up and running

<!-- For Production mode -->
$ CREATE DATABASE jwt;

<!-- For Development Mode -->
$ CREATE DATABASE jwt_dev;

<!-- For Test mode -->
$ CREATE DATABASE jwt_test;
```
  
3. Run the project

```
$ node index.js
```
  
4. Or use `nodemon` for live-reload
  
```
$ npm start
```

> `npm start` will run `nodemon index.js`.
  
5. Navigate to `http://localhost:8000/api-status` in your browser to check you're seing the following response


{ "status": "ok" }
```

> The port can be changed by the setting the environment variable `PORT`

6. If you want to execute the tests

```
$ npm test
```

> `npm test` will run `mocha`.

7. If you want to test it manually you can do it with the following commands

Register a new user:
Method- POST 'http://localhost:8000/users?username=admin&password=admin'


Sign in with the new user credentials:

Method-POST 'http://localhost:8000/auth?username=admin&password=admin'

Copy the token and send a request to get all current users:

Method-GET http://localhost:8000/users -H 'Authorization: Bearer <JWT_TOKEN>

8. And that's it, You should get a similar response to this one, meaning that you're now authenticated

```json
[
  {
        "id": 1,
        "username": "admin2",
        "createdAt": "2021-04-09T13:17:11.000Z",
        "updatedAt": "2021-04-09T13:17:53.000Z"
    }
]
```
